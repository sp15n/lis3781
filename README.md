> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781: Advance Database Management

## Sandro Perez 

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide Screenshots of Installations
    - Create Bitbucket Repo
    - Complete Bitbucket tutorial (bitbutketstationlocations
    - Provide git Command Descriptions)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - N/A, Assignment was skipped due to testing.
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Design a Database using the Oracle Environment 
    - Provide Screenshots of SQL Code
    - Populate the Tables using the Oracle Environment
    - Provide Screenshots of Populated Tables within Oracle
    - Creating Scripts/Queries for the Report 
    - LIS3781's Bitbucket Repo Links  

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Design a Database using the MS SQL Server 
    - Provide Screenshots of SQL Code
    - Populate the Tables using the MS SQL Server
    - Provide Screenshots of Populated Tables within MS SQL
    - Creating Scripts/Queries for the Report 
    - Providing Screenshots (optional)
    - Provide Screenshot of ERD

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Alter A4 Database to include new tables
    - Add Data for new tables
    - Alter exisiting table
    - Add data to the newly changed table 
    - Provide ERD Screenshot
    - Provide SQL Soltions 

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - N/A, Assignment was skipped due to testing.

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Connect and download MongoDB
    - Use MonogoDB on computer's terminal
    - Create a database and collection 
    - Import collection data 
    - Showcase MongoDB commandline knowlegde 