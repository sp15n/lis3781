> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781: Advance Database Management 

## Sandro Perez

### Assignment 1 Requirements:

*Sub-Heading:*

1. Install AMPPS
    a) Provide Screenshots of Installations
2. Answer Theory Questions
3. Entity Relationshop Diagram and SQL Code 
4. Bitbucket Repo Links  
    a) This assigment and 
    b) the completed tutorial (bitbucketstationlocations).
5. Provide git Command Descriptions

#### README.md file should include the following items:

* git Commands with Short Descriptions 
* Installation Proof of AMMPS running 
* Screenshot of A1 ERD
* Ex1. SQL Solutions


> #### Git commands w/short descriptions:

1. git init : Create a local new repository 
2. git add : Add one or more files to a staging
3. git status : List the files you've chnaged and those you still need to add or commit
4. git push :Send chnages to the master branch of your remote repository 
5. git pull : Fetch and merge changes on the remote server to your working directory
6. git commit :  Commit changes to head but not yet to the remote repository 
7. git remote -v : List all currently configured remote repositories

> #### Business Rules:
The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number ofdependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. 
Also, include the following business rules:
- Each employee may have one or more dependents.
- Each employee has only one job.
- Each job can be held by many employees.
- Many employees may receive many benefits.
- Many benefitsmay be selected by many employees (though, while they may not select anybenefits—any dependentsof employees may be on anemployee’s plan).

Notes: Employee/Dependenttables must use suitable attributes. 

In Addition:
- Employee:SSN, DOB, start/end dates, salary;
- Dependent:same information as their associated employee(though, not start/end dates),date added(as dependent),type of relationship: e.g., father, mother, etc.
- Job:title(e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
- Benefit:name(e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
- Plan:type(single, spouse, family), cost, election date(plans must be unique)
- Employeehistory:jobs, salaries, and benefit changes, as well as whomade the changeand why;
- Zero Filled data: SSN, zip codes (not phone numbers: US area codesnot below 201, NJ);
- *All* tables must include notesattribute.

Design Considerations: Generally, avoid using flag values(e.g., yes/no)for status—unless, applicable. Instead, use dateswhen appropriate, as date values provide more information, and sometimes, can be used whena flag value would be used. For example, “null” values in employees’ termination dateswould indicate“active”employees.

In addition, for federal, state, and local mandates, most HR systems require extensive history-keeping. That is, virtually every change made to an employeerecord needs to be logged in a historytable(s)—here, we are keeping the design simple. Also, for reporting (and design) purposes, all *current* data should be kept in the primary table (employee). Every time an employee’s data changes, it should be logged in the history table, including when, why, andwhomade the change—crucial for reporting purposes!


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
