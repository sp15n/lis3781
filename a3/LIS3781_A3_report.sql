-- 1. Display Oracle version(one method).
SELECT * FROM PRODUCT_COMPONENT_VERSION;

-- 2. Display Oracle version(another method).
SELECT * FROM V$VERSION;

-- 3. Display current user.
SELECT user FROM dual;

-- 4. Display current day/time (formatted, and displaying AM/PM)
SELECT TO_CHAR 
    (SYSDATE, 'MM-DD-YYYY HH12:MI:SS AM') "NOW"
    FROM DUAL;
    
--5.Display your privileges.
SELECT * FROM USER_TAB_PRIVS;
SELECT * FROM USER_ROLE_PRIVS;

--6. Display all user tables.
SELECT OBJECT_NAME
FROM USER_OBJECTS
WHERE OBJECT_TYPE = 'TABLE';

--7.Display structure for each table
DESCRIBE customer;
DESCRIBE commodity;
DESCRIBE "order";

-- 8. List the customer number, last name, first name, and e-mail of every customer.
SELECT cus_id, cus_lname, cus_fname, cus_email
FROM customer;

-- 9.Same query as above, include street, city, state, and sort by state in descending order, and last name in ascending order.
SELECT cus_id, cus_lname, cus_fname, cus_street, cus_city, cus_state, cus_email
FROM customer
order by cus_state desc, cus_lname;

-- 10. What is the full name of customer number 3? Display last name first.
SELECT cus_lname, cus_fname
from customer
where cus_id = 3;

-- 11. Find the customer number, last name, first name, and current balance for every customer whose balance exceeds $1,000, sorted by largest to smallest balances.
SELECT cus_id, cus_lname, cus_fname, cus_balance
from customer
where cus_balance > 1000
order by cus_balance desc;

-- 12.List the name of every commodity, and its price (formatted to two decimal places, displaying $ sign), sorted by smallest to largest price.
SELECT com_name, to_char(com_price, 'L99,999.99') as price_formatted 
from commodity
order by com_price;

-- 13. Display all customers� first and last names, streets, cities, states, and zip codes as follows (ordered by zip code descending).
select (cus_lname || ',' || cus_fname) as name,
(cus_street || ', ' || cus_city || ', ' || cus_state || ' ' || cus_zip) as address
from customer
order by cus_zip desc;

-- 14. List all orders not including cereal--use subquery to find commodity id for cereal.
SELECT * FROM "order"
where com_id not in (select com_id from commodity where lower(com_name)='cereal');

-- 15. List the customer number, last name, first name, and balance for every customer whose balance is between $500 and $1,000, (format currency to two decimal places, displaying $ sign).
select cus_id, cus_lname, cus_fname, to_char(cus_balance, '$99,999.99') as balance_formatted
from customer 
where cus_balance >= 500 and cus_balance <= 1000;

-- 16. List the customer number, last name, first name, and balance for every customer whose balance is greater than the average balance, (format currency to two decimal places, displaying $ sign).
select cus_id, cus_lname, cus_fname, to_char(cus_balance, 'L99,999.99') as balance_formatted
from customer
where cus_balance >
    (select avg(cus_balance) from customer);
    
-- 17. List the customer number, name, and *total* order amount for each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an alias �total orders� for the derived attribute.
select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost), 'L99,999.99') as "total orders"
from customer 
    natural join "order"
    group by cus_id, cus_lname, cus_fname
    order by sum(ord_total_cost) desc;

-- 18. List the customer number, last name, first name, and complete address of every customer who lives on a street with "Peach" anywherein the street name.
select cus_id, cus_lname, cus_fname, cus_street, cus_city, cus_state, cus_zip
from customer
where cus_street like '%Peach%';

-- 19. List the customer number, name, and *total* order amount for each customer whose *total* order amount is greater than $1500, for each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an alias �total orders� for the derived attribute
select cus_id, cus_lname, cus_fname, to_char(sum(ord_total_cost, 'L99,999.99') as "total orders"
FROM customer
    natural join "order"
group by cus_id, cus_lname, cus_fname
having sum(ord_total_cost) > 1500
order by sum(ord_total_cost) desc;

-- 20. List the customer number, name, and number of units ordered for orders with 30, 40, or 50 units ordered.
select cus_id, cus_lname, cus_fname, ord_num_units
from customer
    natural join "order"
where ord_num_units IN (30, 40, 50);

-- 21. Using EXISTS operator: List customer number, name, number of orders, minimum, maximum, and sum of theirorder total cost, only if there are 5 or more customers in the customer table, (format currency to two decimal places, displaying $ sign).
select 
cus_id, cus_lname, cus_fname, 
count(*) as "number of orders",
to_char(min(ord_total_cost), 'L99,999.99') as "minimum order cost",
to_char(max(ord_total_cost), 'L99,999.99') as "maximum order cost",
to_char(sum(ord_total_cost), 'L99,999.99') as "total orders"
from customer
natural join "order"
 where exists
 (select count(*) from customer having COUNT(*) >= 5)
 group by cus_id, cus_lname, cus_fname;
 
 -- 22. Find aggregate values for customers:(Note, difference between count(*) and count(cus_balance), one customer does not have a balance.)
 select
 count(*),
 count(cus_balance),
 sum(cus_balance),
 avg(cus_balance),
max(cus_balance),
min(cus_balance)
from customer;

-- 23. Find the number of unique customers who have orders.
 Select distinct count(distinct cus_id) from "order";
 
 --24. List the customer number, name, commodity name, order number, and order amount for each customer order, sorted in descending order amount, (format currency to two decimal places, displaying $ sign), and include an alias �order amount� for the derived attribute.
 select cu.cus_id, cus_lname, cus_fname, com_name, ord_id, to_char(ord_total_cost, 'L99,999.99') as "order amount"
 from customer cu
    join "order" o on o.cus_id=cu.cus_id
    join commodity co on co.com_id=o.com_id
order by ord_total_cost desc;

-- 25. Modify prices for DVD players to $99 
SET DEFINE OFF
UPDATE commodity
set com_price = 99
where com_name = 'DVD & Player';





