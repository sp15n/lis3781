> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781: Advance Database Management 

## Sandro Perez

### Assignment 3 Requirements:

1. Design a Database using the Oracle Environment 
    a) Provide Screenshots of SQL Code
2. Populate the Tables using the Oracle Environment
    b) Provide Screenshots of Populated Tables within Oracle
3. Creating Scripts/Queries for the Report 
    c) Providing Screenshots (optional)
4. LIS3781's Bitbucket Repo Links  

#### README.md file should include the following items:

* Screenshots of the SQL Code
* Screenshot of the populated tables using the Oracle environment 
* (optional) Screenshot of the Required Report
* Report Code and Results
* LIS3781's Bitbucket Repo Link

#### Assignment SQL Code: 
[A3 Solutions SQL](/LIS3781_a3_solutions.sql)

#### Assignment Report SQL: 
[A3 Report SQL](/LIS3781_A3_report.sql)

#### Assignment Report Output: 
[A3 Report Output](/report_results)


#### Assignment Screenshots:

| *Screenshot of SQL CODE*:                                         |
|-------------------------------------------------------------------|
| ![A3 Customer Table](img/cus.png "Customer Table SQL")            |
| ![A3 Commodity Table](img/com.png "Commodity Table SQL")          |
| ![A3 Order Table](img/ord.png "Order Table SQL")                  |
| ![A3 Inserting Table](img/inserting.png "Inserting Values")       |
| ![A3 Viewing Table](img/viewing.png "Viewing Values")             |

| *Screenshot of Populated Tables*:                                 |
|-------------------------------------------------------------------|
| ![A3 Customer Table](img/cus_table.png "Customer Table SQL")      |
| ![A3 Commodity Table](img/com_table.png "Commodity Table SQL")    |
| ![A3 Order Table](img/ord_table.png "Order Table SQL")            |



#### Main ReadMe Bitbucket Link:

*Bitbucket Link:*
[Main LIS3781 Bitbucket Link](https://bitbucket.org/sp15n/lis3781/src/master/ "Main LIS3781 Bitbucket Link")

