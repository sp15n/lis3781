> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781: Advance Database Management 

## Sandro Perez

### Assignment 4 Requirements:

1. Design a Database using the MS SQL Server 
    a) Provide Screenshots of SQL Code
2. Populate the Tables using the MS SQL Server
    b) Provide Screenshots of Populated Tables within MS SQL
3. Creating Scripts/Queries for the Report 
    c) Providing Screenshots (optional)
4. Provide Screenshot of ERD
5. LIS3781's Bitbucket Repo Links  

#### README.md file should include the following items:

* SQL Code
* ERD 
* LIS3781's Bitbucket Repo Link

#### Assignment SQL Code: 
[A4 Solutions SQL](/a4_solutions.sql)

#### Assignment Database SQL: 
[A4 Database SQL](/database.sql)

#### Assignment Data Input SQL: 
[A4 Data Input](/data.sql)

#### Assignment ERD Screenshot:
[A4 ERD](img/erd.png "A4 ERD Image")



#### Business Rules:
A high-volumehome office supply company contractsa database designer to develop a systemin orderto track its day-to-day business operations. The CFO needsan updated method for storing data,running reports, and makingbusiness decisions based upon trends and forecasts, as well as maintaininghistorical datadue to new governmental regulations.Here are the mandatory business rules: 

 - A sales representative has at least one customer, and each customer has at leastone sales repon any given day(as it is a high-volumeorganization).
 - A customer places at least one order. However, each order is placed by only one customer.
 - Each order contains at least one order line.Conversely, each order line is contained in exactly one order.
 - Each product may be on a number of order lines.Though, each order line containsexactly one productid (though, each product idmay have a quantity of more than one included, e.g., “oln_qty”).
 - Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).• An invoice can have one (full),or can have many payments (partial). Though, each payment is made to only one invoice.• A store hasmany invoices, but each invoice is associated with only one store.• A vendor providesmany products, but each product is provided by only one vendor.• Must track yearly history of sales reps, including(also, see Entity-specific attributesbelow): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents).• Must track history of products, including: cost, price, and discount percentage (if any).

 #### Notes:
 -  A customer’s contact (in-store or online) is made through a sales rep.
 -  A customer buys or potentially buys products from the company, but does not have to.
 - An order is a purchase of one or more products by a customer.If an order is cancelled, it is deleted (optional participation).
 -  An order line contains the details about each product sold on a particular customer order, and includes data such as quantityand price.
 - A product is an item that the company sells that was initially bought from an outside vendor (whichmay also be the manufacturer).
 - A sales rep receives a 3% commission based upon the amount of year-to-date sales.
 - A sales reps’s current yearly sales goalis 8% more than theirprevious year’s total sales.

 #### Additional Notes:
 - Social security numbers, should be unique, and hashedandsaltedfor security purposes.
 - ERD MUST include relationships,though,notcardinalities.
 

#### Main ReadMe Bitbucket Link:

*Bitbucket Link:*
[Main LIS3781 Bitbucket Link](https://bitbucket.org/sp15n/lis3781/src/master/ "Main LIS3781 Bitbucket Link")

