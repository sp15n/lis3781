-- Inserting Into Person Table 
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES 
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', 984208440, 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', 's', NULL),
(4, NULL, 'Jane', 'Thomspson', 'f', '1978-05-08', '13563 Ocean View Drive', 'Seattle', 'WA', 132084409, 'jthompson@gmail.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f', '1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', 's', NULL),
(6, NULL, 'Tony', 'Smith', 'm', '1972-05-04', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', 'c', NULL),
(7, NULL, 'Hank', 'Pymi', 'm', '1980-08-28', '2355 Brown Street', 'Cleveland', 'OH', 822348890, 'hpym@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '4902 Avendale Avenue', 'Scottsdale', 'AV', 872638332, 'bbest@yahoo.com', 'c', NULL),
(9, NULL, 'Sandra', 'Smith', 'f', '1990-01-26', '87912 Lawrence Ave', 'Atlanta', 'GA', 672348890, 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Avenue', 'Miami', 'FL', 342219932, 'acurry@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', 'c', NULL),
(13, NULL, 'Adam', 'Smith', 'm', '1995-01-31', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f', '1970-03-22', '56343 Rover Ct.', 'Bilings', 'MT', 672048823, 'jsleen@symaptico.com', 'c', NULL),
(15, NULL, 'Bill', 'Neiderheim', 'm', '1982-06-13', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', 'c', NULL);
GO

-- Data for slsrep table
INSERT INTO dbo.slsrep
(pre_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, NULL),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);

Select * from dbo.slsrep;

--Data for Customer Table 
INSERT INTO dbo.customer 
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, NULL),
(9, 981.73, 1672.38, 'Customer always pays on time.'),
(10, 541.23, 782.57, 'High Balance.'),
(11, 251.02, 13782.96, NULL),
(12, 582.67, 963.12, 'Good Customer.'),
(13, 121.67, 1057.45, 'Previously Paid in Full.'),
(14, 765.43, 6789.42, NULL),
(15, 304.39, 456.81, NULL);

Select * from dbo.customer;

--Data for table contact
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES 
(1, 6, '1999-01-01', NULL),
(2, 6, '2001-09-29', NULL),
(3, 7, '2002-08-15', NULL),
(2, 7, '2002-09-01', NULL),
(4, 7, '2004-01-05', NULL),
(5, 8, '2004-02-28', NULL),
(4, 8, '2004-03-03', NULL),
(1, 9, '2004-04-07', NULL),
(5, 9, '2004-04-07', NULL),
(3, 11, '2005-05-02', NULL),
(4, 13, '2005-06-14', NULL),
(2, 15, '2005-07-02', NULL);

Select * from dbo.contact;

-- Data for order table
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-12-24', NULL),
(4, '2012-10-23', '2012-12-24', NULL),
(5, '2013-11-23', '2013-11-24', NULL),
(6, '2014-12-23', '2014-12-24', NULL),
(7, '2015-02-23', '2015-02-24', NULL),
(8, '2016-03-23', '2016-03-24', NULL),
(9, '2017-04-23', '2017-04-24', NULL),
(10, '2018-05-23', '2018-05-24', NULL);

Select * from dbo.[order];

-- Data for store table 
INSERT INTO dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES 
('Walgreens', '14567 Walnut Ln', 'Aspen', 'IL', '475315690', '3127658127', 'info@walgreens.com', 'www.walgreens.com', NULL),
('CVS', '572 Casper Rd', 'Chicago', 'IL', '47523690', '3127238127', 'info@cvs.com', 'www.cvs.com', NULL),
('Lowes', '1234 Catapult Ln', 'Clover', 'WA', '428415690', '3120928127', 'info@lowes.com', 'www.lowes.com', NULL),
('Walmart', '21 Walnut Ln', 'St. Louis', 'FL', '475315694', '3117658127', 'info@walmart.com', 'www.walmart.com', NULL),
('Dollar General', '12349 David Ln', 'Detroit', 'MI', '475317790', '2137658127', 'info@dollargeneral.com', 'www.dollargeneral.com', 'recently sold property.');

Select * from dbo.store;

-- Data for Invoice Table
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES 
(5, 1, '2001-05-03', 58.32, 0, NULL),
(4, 1, '2002-05-03', 528.32, 0, NULL),
(1, 1, '2003-05-03', 18.32, 0, NULL),
(3, 2, '2004-05-03', 20.32, 0, NULL),
(2, 3, '2005-05-03', 258.30, 0, NULL),
(6, 4, '2006-05-03', 1158.32, 0, NULL),
(7, 5, '2007-05-03', 898.32, 0, NULL),
(8, 3, '2008-05-03', 5118.21, 0, NULL),
(9, 2, '2009-05-03', 508.12, 0, NULL),
(10, 4, '2011-05-03', 898.78, 0, NULL);

Select * from dbo.invoice;

-- Data for Vendor Table 
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES 
('Sysco', '14567 Walnut Ln', 'Aspen', 'IL', '475315690', '3127658127', 'sales@sysco.com', 'www.sysco.com', NULL),
('General Electric', '572 Casper Rd', 'Chicago', 'IL', '47523690', '3127238127', 'info@generalelectric.com', 'www.generalelectric.com', NULL),
('Goodyear', '1234 Catapult Ln', 'Clover', 'WA', '428415690', '3120928127', 'info@goodyear.com', 'www.goodyear.com', NULL),
('Snap-on', '21 Walnut Ln', 'St. Louis', 'FL', '475315694', '3117658127', 'info@snapon.com', 'www.snapon.com', NULL),
('Cisco', '12349 David Ln', 'Detroit', 'MI', '475317790', '2137658127', 'info@cisco.com', 'www.cisco.com', 'Good Turnover');

Select * from dbo.vendor;

-- Data for Product table 
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES 
(1, 'hammer', 2.5, 45, 7.99, 30, NULL),
(2, 'screwdriver', 2.8, 120, 1.99, 49, NULL),
(4, 'pail', 15, 73, 8.50, 40, NULL),
(5, 'cooing oil', 2.5, 23, 19.99, 28.88, NULL),
(3, 'frying pan', 2.5, 22, 7.99, 50, NULL);

Select * from dbo.product;

-- data for order_line table 
INSERT INTO dbo.order_line 
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(3, 4, 3, 6.99, NULL),
(4, 1, 2, 12.76, NULL),
(5, 5, 13, 58.99, NULL);

Select * from dbo.order_line;


--CONTINUE HERE
--data from payment table 
INSERT INTO dbo.payment 
(inv_id, pay_date, pay_amt, pay_notes)
(5, '2008-07-01', 5.99, NULL),
(4, '2008-07-01', 4.99, NULL),
(1, '2008-07-01', 6.99, NULL),
(3, '2008-07-01', 5.99, NULL),
(2, '2008-07-01', 3.99, NULL),
(6, '2008-07-01', 10.99, NULL),
(8, '2008-07-01', 3.99, NULL),
(7, '2008-07-01', 5.99, NULL),
(10, '2008-07-01', 6.99, NULL),
(4, '2008-07-01', 1.99, NULL);

-- Data for the product history table 
INSERT INTO dbo.product_hist
(pro_id, pht_date_ pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 9.99, 30, NULL),
(2, '2006-01-04 11:52:34', 5.99, 19.99, 30, NULL),
(3, '2007-01-05 11:56:34', 6.99, 3.99, 30, NULL),
(4, '2008-01-06 11:57:34', 7.99, 4.99, 30, NULL),
(5, '2009-01-07 11:59:34', 8.99, 19.99, 30, NULL);

-- Data for sales rep history
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
VALUES 
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(2, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(4, 'u', getDate(), ORGINIAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORGINIAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);


