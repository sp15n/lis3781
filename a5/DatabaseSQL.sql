-- Database SQL


-- Creating Region Table
IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
GO

CREATE TABLE dbo.region
(
    reg_id TINYINT NOT NULL IDENTITY(1,1),
    reg_name CHAR(1) NOT NULL,
    reg_notes VARCHAR(255) NULL,
    PRIMARY KEY (reg_id)
);
GO


-- Creating State Table
IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
GO

Create Table dbo.state 
(
    ste_id TINYINT NOT NULL IDENTITY(1,1),
    reg_id TINYINT NOT NULL,
    ste_name CHAR(2) NOT NULL DEFAULT 'FL',
    ste_notes VARCHAR(255) NULL,
    PRIMARY KEY (ste_id),

    CONSTRAINT fk_state_region
    FOREIGN KEY (reg_id)
    REFERENCES dbo.region (reg_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE 
);
GO

-- Creating Table City 
IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
    cty_id SMALLINT NOT NULL IDENTITY(1,1),
    ste_id TINYINT NOT NULL,
    cty_name VARCHAR(30) NOT NULL,
    cty_notes VARCHAR(255) NULL,
    PRIMARY KEY (cty_id),

    CONSTRAINT fk_city_state
    FOREIGN KEY (ste_id)
    REFERENCES dbo.state (ste_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- Creating Store Table 
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL IDENTITY(1,1),
    cty_id SMALLINT NOT NULL,
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_zip INT NOT NULL CHECK (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint NOT NULL CHECK (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id),

    CONSTRAINT fk_store_city
    FOREIGN KEY (cty_id)
    REFERENCES dbo.city (cty_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- Creating Time Table
IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time
(
    tim_id INT NOT NULL IDENTITY(1,1),
    tim_yr SMALLINT NOT NULL,
    tim_qtr TINYINT NOT NULL,
    tim_month TINYINT NOT NULL,
    tim_week TINYINT NOT NULL,
    tim_day TINYINT NOT NULL,
    tim_time TIME NOT NULL,
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

-- Creating Sale Table
IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale 
(
    pro_id SMALLLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    UNIQUE NONCLUSTERED (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
    FOREIGN KEY (tim_id)
    REFERENCES dbo.time (tim_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

--Inserting Data into Region
INSERT INTO region
(reg_name, reg_notes)
VALUES
('c',NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);
GO
select * from dbo.region;

-- Inserting Data into State
INSERT INTO state 
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'Il', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL);
GO

Select * from dbo.store;

--Inserting into City Table
INSERT INTO city
(ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(2, 'Chicago', NULL),
(3, 'Clover', NULL),
(4, 'St. Louis', NULL);
GO

Select * from dbo.city;

--Inserting into Time Table
INSERT INTO dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(2010, 4, 12, 49, 4, '08:59:59', NULL),
(1999, 4, 12, 52, 5, '05:59:59', NULL),
(2001, 4, 8, 36, 1, '09:59:59', NULL),
(2008, 3, 7, 27, 2, '23:59:59', NULL);
GO
select * from dbo.time;

-- Inserting Data into Sales Table 
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 5, 5, 3, 20, 9.99, 199.8, NULL),
(2, 1, 4, 2, 5, 5.99, 29.8, NULL),
(3, 2, 3, 1, 30, 19.99, 1.8, NULL),
(4, 3, 2, 2, 10, 10.99, 19.8, NULL),
(5, 2, 1, 3, 25, 11.99, 20.8, NULL),
(1, 1, 5, 4, 22, 2.99, 18.8, NULL),
(2, 5, 3, 2, 23, 12.99, 12.8, NULL),
(3, 3, 2, 1, 24, 3.99, 21.8, NULL),
(4, 4, 4, 3, 12, 18.99, 30.8, NULL),
(5, 2, 1, 4, 14, 12.99, 9.8, NULL),
(1, 2, 5, 2, 14, 11.99, 10.8, NULL),
(2, 2, 4, 3, 23, 9.99, 178.8, NULL),
(3, 1, 3, 1, 21, 10.99, 121.8, NULL),
(4, 1, 2, 3, 19, 11.99, 1.8, NULL),
(5, 1, 1, 3, 10, 22.99, 99.8, NULL),
(1, 2, 5, 3, 2, 2.99, 9.8, NULL),
(2, 4, 4, 2, 13, 21.99, 6.8, NULL),
(3, 3, 3, 4, 15, 4.99, 9.8, NULL),
(4, 3, 2, 2, 15, 5.99, 7.8, NULL),
(5, 2, 1, 1, 10, 6.99, 21.8, NULL),
(1, 2, 5, 1, 2, 6.99, 199.8, NULL),
(2, 1, 4, 3, 22, 7.99, 12.8, NULL),
(3, 3, 3, 2, 40, 8.99, 86.8, NULL),
(4, 2, 2, 2, 30, 9.99, 15.8, NULL),
(5, 5, 1, 3, 10, 91.99, 41.8, NULL);
GO

select * from sale;











