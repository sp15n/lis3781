> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781: Advance Database Management 

## Sandro Perez

### Assignment 5 Requirements:

1. Alter an Exisiting Database using the MS SQL Server 
2. Populate the Existing Tables using the MS SQL Server
3. Add New Tables to Exisiting Database 
4. Provide Screenshot of ERD
5. Provide SQL Solutions 
6. LIS3781's Bitbucket Repo Links  

#### README.md file should include the following items:

* SQL Code
* ERD 
* LIS3781's Bitbucket Repo Link

#### Assignment SQL Code: 
[A5 Solutions SQL](/solutions.sql)

#### Assignment Database SQL: 
[A5 Database SQL](/DatabaseSQL.sql)

#### Assignment ERD Screenshot:
[A5 ERD](img/erd.png "A5 ERD Image")



#### Business Rules:
Expanding upon the high-volumehome office supply company’s data tracking requirements, the CFO requests your services again to extend the data model’s functionality. The CFO has read about the capabilities of data warehousing analytics and business intelligence (BI), and is looking to develop a smaller data mart as a test platform. He is under pressure from the members of the company’s board of directors who want to reviewmore detailed salesreports based upon the following measurements:

1. Product
2. Customer
3. Sales Rep
4. Time 
5. Location

Furthermore, the board members want location to be expanded to include the following characteristics of location:
 1. Region
 2. State
 3. City
 4. Store

 

#### Main ReadMe Bitbucket Link:

*Bitbucket Link:*
[Main LIS3781 Bitbucket Link](https://bitbucket.org/sp15n/lis3781/src/master/ "Main LIS3781 Bitbucket Link")

