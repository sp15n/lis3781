--1 Create a stored procedure (sp_product_days_of_week) 
-- listing the product names, descriptions, and the day of the week in which they were sold, in ascending order of the day of week.

IF OBJECT_ID(N'dbo.sp_product_days_of_week', N'P') IS NOT NULL
DROP PROC dbo.sp_product_days_of_week;
GO
CREATE PROC dbo.sp_product_days_of_week AS 
BEGIN
  select pro_name, pro_descript, datename(dw, tim_day) 'day_of_week'
  from product p
    join sale s on p.pro_id=s.pro_id
    join time t on t.tim_id=s.tim_id
  order by tim_day asc; -- sorts numerically, rather than string return of datename() function
END
GO
-- call stored procedure
exec dbo.sp_product_days_of_week;

--2 Create a stored procedure (sp_product_drill_down) 
-- listing the product name, quantity on hand, store name, city name, state name, and region name where each product was purchased, in descending order of quantity on hand.

IF OBJECT_ID(N'dbo.sp_product_drill_down', N'P') IS NOT NULL
DROP PROC dbo.sp_product_drill_down;
GO
CREATE PROC dbo.sp_product_drill_down AS 
BEGIN
  select pro_name, pro_qoh, 
  FORMAT(pro_cost, 'C', 'en-us') as cost, 
  FORMAT(pro_price, 'C', 'en-us') as price, 
  str_name, cty_name, ste_name, reg_name
  from product p
    join sale s on p.pro_id=s.pro_id
    join store sr on sr.str_id=s.str_id
    join city c on sr.cty_id=c.cty_id
    join state st on c.ste_id=st.ste_id 
    join region r on st.reg_id=r.reg_id
  order by pro_qoh desc;
END
GO
-- call stored procedure
exec dbo.sp_product_drill_down;

--3 Create a stored procedure (sp_add_payment) that adds a payment record. Use variables and pass suitable arguments.

IF OBJECT_ID(N'dbo.sp_add_payment', N'P') IS NOT NULL
DROP PROC dbo.sp_add_payment;
GO
CREATE PROC dbo.sp_add_payment 
  @inv_id_p int,
  @pay_date_p datetime,
  @pay_amt_p decimal(7,2),
  @pay_notes_p varchar(255)
AS 
BEGIN
-- don't need pay_id pk, because it is auto-increment
  insert into payment(inv_id, pay_date, pay_amt, pay_notes)
  values
  (@inv_id_p, @pay_date_p, @pay_amt_p, @pay_notes_p);
END
GO
  -- initialize (i.e., declare and assign values to) variables
  DECLARE 
  @inv_id_v int = 6,
  @pay_date_v DATETIME = '2014-01-05 11:56:38',
  @pay_amt_v DECIMAL(7,2) = 159.99,
  @pay_notes_v VARCHAR(255) = 'testing sp_add_payment';
-- call stored procedure
exec dbo.sp_add_payment @inv_id_v, @pay_date_v, @pay_amt_v, @pay_notes_v;

--4 Create a stored procedure (sp_customer_balance) 
--listing the customer’s id, name, invoice id, total paid on invoice, balance (derived attribute from the difference of a customer’s invoice total and their respective payments), pass customer’s last name as argument—which may return more than one value.

IF OBJECT_ID(N'dbo.sp_customer_balance', N'P') IS NOT NULL
DROP PROC dbo.sp_customer_balance
GO
CREATE PROC dbo.sp_customer_balance 
  @per_lname_p varchar(30)
AS 
BEGIN
  select p.per_id, per_fname, per_lname, i.inv_id,
    FORMAT(sum(pay_amt), 'C', 'en-us') as total_paid, 
    FORMAT((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
    from person p
      join dbo.customer c on p.per_id=c.per_id 
      join dbo.contact ct on c.per_id=ct.per_cid
      join dbo.[order] o on ct.cnt_id=o.cnt_id
      join dbo.invoice i on o.ord_id=i.ord_id
      join dbo.payment pt on i.inv_id=pt.inv_id
     -- must be contained in group by, if not used in aggregate function
  where per_lname=@per_lname_p
  group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
END
GO
DECLARE @per_lname_v varchar(30) = 'smith';
-- call stored procedure
exec dbo.sp_customer_balance @per_lname_v;

--5 Create and display the results of a stored procedure (sp_store_sales_between_dates) 
-- that lists each store's id, sum of total sales (formatted), and years for a given time period, by passing the start/end dates, group by years, and sort by total sales then years, both in descending order.
IF OBJECT_ID(N'dbo.sp_store_sales_between_dates', N'P') IS NOT NULL
DROP PROC dbo.sp_store_sales_between_dates
GO
-- create stored procedure w/parameters
CREATE PROC dbo.sp_store_sales_between_dates 
  @start_date_p date,
  @end_date_p date
AS 
BEGIN
  select st.str_id, FORMAT(sum(sal_total), 'C', 'en-us') as 'total sales', year(tim_yr) as year
  from store st
    join sale s on st.str_id=s.str_id
    join time t on s.tim_id=t.tim_id
  where tim_yr between @start_date_p and @end_date_p
  group by year(tim_yr), st.str_id
  order by sum(sal_total) desc, year(tim_yr) desc;
END
GO
  DECLARE 
  @start_date_v date = '2010-01-01',
  @end_date_v date = '2013-12-31';
-- call stored procedure
exec dbo.sp_store_sales_between_dates @start_date_v, @end_date_v;

--6 Create a trigger (trg_check_inv_paid) that updates an invoice record, after a payment has been made, indicating whether or not the invoice has been paid.

IF OBJECT_ID(N'dbo.trg_check_inv_paid', N'TR') IS NOT NULL
DROP TRIGGER dbo.trg_check_inv_paid
GO
CREATE TRIGGER dbo.trg_check_inv_paid 
ON dbo.payment
AFTER INSERT AS
BEGIN
-- only use for testing: reset all paid invoices to unpaid (0)
  update invoice
  set inv_paid=0;
  UPDATE invoice
  SET inv_paid=1
  FROM invoice as i
    JOIN 
    (
      SELECT inv_id, sum(pay_amt) as total_paid
      FROM payment
      GROUP BY inv_id
    ) as v ON i.inv_id=v.inv_id
  WHERE total_paid >= inv_total;
-- NOTE: v is virtual table (query result set) created by SELECT statement
END
GO
-- fire trigger
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes) 
VALUES 
(3, '2014-07-04', 75.00, 'Paid by check.');

--7 Create and display the results of a stored procedure (sp_order_line_total) 
-- that calculates the total price for each order line, based upon the product price times quantity, which yields a subtotal (oln_price), total column includes 6% sales tax. Query result set should display order line id, product id, name, description, price, order line quantity, subtotal (oln_price), and total with 6% sales tax. Sort by product ID.

IF OBJECT_ID(N'dbo.sp_order_line_total', N'P') IS NOT NULL
DROP PROC dbo.sp_order_line_total
GO
CREATE PROC dbo.sp_order_line_total AS 
BEGIN
  select oln_id, p.pro_id, pro_name, pro_descript, 
  FORMAT(pro_price, 'C', 'en-us') as pro_price, 
  oln_qty,
  FORMAT((oln_qty * pro_price), 'C', 'en-us') as oln_price,
  FORMAT((oln_qty * pro_price) * 1.06, 'C', 'en-us')  as total_with_6pct_tax
  from product p
  join order_line ol on p.pro_id=ol.pro_id
  order by p.pro_id;
END
GO
-- call stored procedure
exec dbo.sp_order_line_total;