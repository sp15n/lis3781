USE sp15n;

-- Table Person
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
per_id SMALLINT UNSIGHED NOT NULL AUTO_INCREMENT,
per_ssn BINARY(64) NULL,
per_salt BINARY(64) NULL COMMENT,
per_fname VARCHAR(15) NOT NULL,
per_lname VARCHAR(30) NOT NULL,
per_street VARCHAR(30) NOT NULL,
per_city VARCHAR(30) NOT NULL,
per_state CHAR(2) NOT NULL,
per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
per_email VARCHAR(100) NOT NULL,
per_dob DATE NOT NULL,
per_type ENUM('a','c','j') NOT NULL,
per_notes VARCHAR(255) NULL,
PRIMARY KEY(per_id),
UNIQUE INDEX ux_per_ssn (per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE= utf8_unicode_ci;

SHOW WARNINGS;

-- Data for table person
START TRANSACTION;

INSERT INTO PRECISION
(per_id,per_ssn, per_salt, per_lname, per_fname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES 
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Drive', 'Rochester','NY',324402222,'srodgers@comcast.net', '1923-10-03','c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive','Gotham', 'NY', 003208440, 'bwayne@knology.net','1968-03-20','c',NULL),
(NULL, NULL, NULL, 'Peter','Paker', '20 Ingram Street','New York','NY', 102862341, 'ppaker@msn.com', '1988-09-12','c', NULL),
(NULL, NULL, NULL, 'Jane', ' Thompson', '13563 Ocean View Drive', 'Seattle','WA',032084409,'jthompson@gmail.com','1978-05-08','c',NULL),
(NULL, NULL, NULL, 'Debra','Steele','543 Oak Ln','Milwaukee','WI',286234178,'dsteele@verizon.net', '1994-07-19','c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark','332 Palm Avenue','Malibu','CA',902638332,'tstark@yahoo.com','1972-05-04','a',NULL),
(NULL, NULL, NULL, 'Hank','Pymi','2355 Brown Street','Cleveland','OH',022348890,'hpym@aol.com','1980-08-28','a',NULL),
(NULL, NULL, NULL, 'Bob','Best','4902 Avendale Avenue','Scttsdale','AZ',872638332,'bbest@yahoo.com','1992-02-10','a',NULL),
(NULL, NULL, NULL, 'Sandra','Dole','87912 Lawrence Ave','Atlanta','GA',002348890,'sdole@gmail.com','1990-01-26','a',NULL),

