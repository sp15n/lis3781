> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781: Advance Database Management 

## Sandro Perez

### Project 2 Requirements:

1. Create a Free MongoDB Account  
2. Create a cluster and connect to it via computer's terminal (mac)
3. Create a Database named 'test' and a collection within it named 'restaurant'
4. Upload/mport a JSON data file to the new collection 
5. Provide a screenshot of atleast one MongoDB shell command
6. LIS3781's Bitbucket Repo Links  

#### README.md file should include the following items:

* Atleast one Screenshot of a MonogDB Shell Command
* Optional JSON Code for Report
* LIS3781's Bitbucket Repo Link

#### MongoDB Shell Command Project Screenshot: 
![P2 Command Shell](img/p2.png)




#### Report Ran:
1. Displayall documents in collection.
2. Display the number of documents in collection.
3. Retrieve 1st 5 documents.
4. Retrieve restaurants in the Brooklyn borough. 
5. Retrieve restaurants whose cuisine is American.
6. Retrieve restaurants whose borough is Manhattanand cuisine is hamburgers.
7. Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers.
8. Query zipcode field in embedded address document.Retrieve restaurants in the 10075 zip code area.
9. Retrieve restaurants whose cuisine is chicken and zip code is 10024.
10. Retrieve restaurants whose cuisine is chicken or whose zip code is 10024.
11. Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sortby descending order of zipcode.
12. Retrieve restaurants with a grade A.
13. Retrieve restaurants with a grade A, displaying only collectionid, restaurant name, and grade.
14. Retrieve restaurants with a grade A, displaying only restaurant name, and grade(no collection id):
15. Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending.
16. Retrieve restaurants with a score higher than 80.
17. Insert a record with the following data:
street = 7th 
Avenuezip code = 10024
building = 1000
coord = -58.9557413, 31.7720266
borough = Brooklyn
cuisine = BBQ
date = 2015-11-05T00:00:00Z
grade" : C
score = 15
name = Big Tex
restaurant_id = 61704627
18. Update the following record: Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the last modified field with the current date.
19. Delete the following records:Delete all White Castle restaurants.

#### Main ReadMe Bitbucket Link:

*Bitbucket Link:*
[Main LIS3781 Bitbucket Link](https://bitbucket.org/sp15n/lis3781/src/master/ "Main LIS3781 Bitbucket Link")

